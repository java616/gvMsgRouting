(function(){
    var root = typeof self == 'object' && self.self === self && self ||
        typeof global == 'object' && global.global === global && global ||
        this;
    var args = root.arguments;
    var command = args.pop();
    var s = new java.net.Socket("localhost", 9091);
    var os = new java.io.PrintWriter(s.getOutputStream(), true);
    var is = new java.io.BufferedReader(new java.io.InputStreamReader(s.getInputStream()));
    var write = function(str){
        new java.io.PrintWriter(os).println(str);
        os.flush();
    };
    if(command) write(command);
    var connected = true;
    while(connected){
        var msg = is.readLine();
        if(msg=="#END#"){
            print(msg);
            connected =false;
        }else if(msg==null) {
            connected = false;
        } else {
            print(msg);
        }
    }
})();