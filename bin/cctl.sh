#!/usr/bin/env bash

function check_java (){
    JAVA_PATH=`which java 2>/dev/null`
    if [ "x$JAVA_PATH" != "x" ]; then
        _javacmd=java
    elif [[ -n "$JAVA_HOME" ]] && [[ -x "$JAVA_HOME/bin/java" ]];  then
        _javacmd="$JAVA_HOME/bin/java"
    else
        echo "ERROR: java Not find!"
    fi

    if [[ "$_javacmd" ]]; then
        version=$("$_javacmd" -version 2>&1 | awk -F '"' '/version/ {print $2}')
        if [[ "$version" < "1.6" ]]; then
            echo "download rhino.jar"
        elif [[ "$version" < "1.8" ]]; then
            jsi='jrunscript -f'
            WITH_ARGS=''
        else
            jsi='jjs -scripting'
            WITH_ARGS='--'
        fi
    fi
}
check_java

$jsi client.js $WITH_ARGS $@
