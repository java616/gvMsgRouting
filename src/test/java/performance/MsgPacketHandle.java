package performance;


import com.gvsoft.communication.server.model.inf.IPacket;
import com.gvsoft.communication.server.model.order.MsgOrder;
import com.gvsoft.communication.server.net.handle.PacketHandle;
import com.gvsoft.communication.server.net.socket.NSocket;
import com.gvsoft.communication.server.net.socket.NSocketManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created with IntelliJ IDEA.
 * ProjectName:gvMsgRouting
 * Author: zhaoqiubo
 * Date: 15/9/29
 * Time: 上午9:56
 * Desc:
 */

public class MsgPacketHandle extends PacketHandle {
    private static Logger logger = LogManager.getLogger("server");
    public void handlePacket(IPacket iPacket, NSocket nSocket) {
        super.handlePacket(iPacket, nSocket);

        MsgInfo msgInfo = new MsgInfo();
        msgInfo = msgInfo.generaterMsgInfo(iPacket.getPacketBody());
        NSocket targetSocket = NSocketManager.getConnByUserId(msgInfo.getReceiver());
        if (targetSocket!=null && targetSocket.getChannel().isOpen()) {
            MsgOrder msgOrder = new MsgOrder(iPacket.getPacketBody());
            targetSocket.write(msgOrder);
        } else {

            /*
            此处将数据放入离线存储队列
             */
            if (logger.isDebugEnabled()) {
                logger.debug("短消息放入离线短消息队列：<" + msgInfo.getReceiver() + ">");
            }
        }

    }
}