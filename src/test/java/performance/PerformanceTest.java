package performance;

import com.gvsoft.communication.client.Client;
import com.gvsoft.communication.client.ClientConfig;
import com.gvsoft.communication.client.model.order.ClientMsgOrder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.atomic.AtomicLong;

/**
 * Created with IntelliJ IDEA.
 * ProjectName:gvMsgRouting
 * Author: zhaoqiubo
 * Date: 15/9/15
 * Time: 上午8:49
 * Desc: 性能测试类
 */
class Task extends Thread {
    private final Client client;
    private final AtomicLong counter;
    private final long startTime;
    private final long n;

    private final static Logger logger = LogManager.getLogger("client");

    public Task(Client client, AtomicLong counter, long startTime, long n) {
        this.client = client;
        this.counter = counter;
        this.startTime = startTime;
        this.n = n;
    }

    @Override
    public void run() {
        for (int i = 0; i < n; i++) {
            MsgInfo msg = new MsgInfo(client.getUserId(), "18604055343", ClientConfig.systemTimeUtc(), "hello，赵大侠");
            ClientMsgOrder order = new ClientMsgOrder(client.getToken(), msg);

            if (client.isActive()) {
                client.write(order);
                counter.incrementAndGet();
            }

            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                logger.error(e.getMessage());
            }
            if ((System.currentTimeMillis() - startTime) % 1000 == 0) {
                double qps = counter.get() * 1000.0 / (System.currentTimeMillis() - startTime);
                double total = counter.get();
                System.out.format("QPS: %.2f\n", qps);
//                System.out.format("Total: %.2f\n", total);
            }
        }
    }
}

public class PerformanceTest {

    public static void main(String arg[]) {
        Integer clientCount = 200;
        Client[] client = new Client[clientCount];
        for (int i = 0; i < clientCount; i++) {
            client[i] = new Client();
            if(i%2==0) {
                client[i].start("欧诺个户" + i, System.getProperty("user.dir") + "/target/classes/client_cfg.properties");
            }else{
                client[i].start("欧诺个333户" + i, System.getProperty("user.dir") + "/target/classes/client_cfg2.properties");
            }
        }
        Client.regConsole(System.getProperty("user.dir") + "/target/classes/common_client_cfg.properties");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        final long startTime = System.currentTimeMillis();
        final AtomicLong counter = new AtomicLong(0);
        for (Client c : client) {
            System.out.println("client状态:" + c.getUserId() + "，" + c.getClientStatus());
        }

        Task[] tasks = new Task[client.length];
        for (int i = 0; i < client.length; i++) {
            if (client[0].isActive()) {
                tasks[i] = new Task(client[i], counter, startTime, 1000000);
            }
        }

        for (Task task : tasks) {
            if (task != null) {
                task.start();
            }
        }
    }

}
