package performance;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * Created with IntelliJ IDEA.
 * ProjectName:gvMsgClient
 * Author: zhaoqiubo
 * Date: 15/8/13
 * Time: 下午3:21
 * Desc:
 */
public class MsgInfo extends Object {

    //短消息级别报文的队列
    public static BlockingQueue MQUEUE = new ArrayBlockingQueue<MsgInfo>(1000);

    private String sender;
    private String receiver;

    private int sendTime;
    private int receiveTime;
    private String msgBody;

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public int getSendTime() {
        return sendTime;
    }

    public void setSendTime(int sendTime) {
        this.sendTime = sendTime;
    }

    public int getReceiveTime() {
        return receiveTime;
    }

    public void setReceiveTime(int receiveTime) {
        this.receiveTime = receiveTime;
    }

    public String getMsgBody() {
        return msgBody;
    }

    public void setMsgBody(String msgBody) {
        this.msgBody = msgBody;
    }
    public MsgInfo(String sender, String receiver, int sendTime, String msgBody) {
        this.sender = sender;
        this.receiver = receiver;
        this.sendTime = sendTime;
        this.msgBody = msgBody;
    }

    public MsgInfo(){}


    public String toJsonStr(){
        return JSON.toJSONString(this, SerializerFeature.WriteMapNullValue);
    }

    public MsgInfo generaterMsgInfo(String jsonStr){
        return (MsgInfo)JSON.parseObject(jsonStr,MsgInfo.class);
    }

    public void sendMsg(){

    }
}