package performance;

import com.gvsoft.communication.server.Server;

/**
 * Created with IntelliJ IDEA.
 * ProjectName:gvMsgRouting
 * Author: zhaoqiubo
 * Date: 15/9/29
 * Time: 上午10:17
 * Desc:
 */
public class PerformanceServer {
    public static void main(String[] arg){
        Server server = Server.getInstance();
        server.start(arg);
        server. registerHandle("M", new MsgPacketHandle());
    }

}
