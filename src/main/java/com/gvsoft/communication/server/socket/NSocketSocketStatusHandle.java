package com.gvsoft.communication.server.socket;

import com.gvsoft.communication.common.model.order.TokenOrder;
import com.gvsoft.communication.net.NSocket;
import com.gvsoft.communication.net.status.ISocketStatusHandle;

import java.util.UUID;

/**
 * Created with IntelliJ IDEA.
 * ProjectName:MService
 * Author: zhaoqiubo
 * Date: 15/11/23
 * Time: 上午10:20
 * Desc:
 */
public class NSocketSocketStatusHandle implements ISocketStatusHandle {
    @Override
    public void afterClosed(NSocket nSocket) {
        //释放管理类中的NSocket
        if (null == nSocket.getUserId()) {
            NSocketManager.removeUnregNSocket(nSocket.getToken());
        } else {
            NSocketManager.removeNSocket(nSocket.getToken(), nSocket.getUserId());
        }
    }

    @Override
    public void afterActived(NSocket nSocket) {

    }

    @Override
    public void afterNew(NSocket nSocket) {

        String token = UUID.randomUUID().toString().replace("-", "");
        nSocket.setToken(token);
        TokenOrder tokenOrder = new TokenOrder(token);
        nSocket.write(tokenOrder);
        NSocketManager.addUnregNSocket(nSocket);
    }

    @Override
    public void afterReconnect(NSocket nSocket) {

    }

    @Override
    public void afterConnected(NSocket nSocket) {

    }
}
