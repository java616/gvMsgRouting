package com.gvsoft.communication.server.serverstatus;

import com.gvsoft.communication.net.NSocket;

/**
 * Created with IntelliJ IDEA.
 * ProjectName:MService
 * Author: zhaoqiubo
 * Date: 15/10/19
 * Time: 下午4:15
 * Desc: NSocket状态监听接口
 */
public interface IStatusManageHandle {
    /**
     * 添加这个NSocket到缓冲区之前会被调用
     * @param nSocket 将要被添加到管理缓冲区的NSocket
     */
    void beforeAdded(NSocket nSocket);
    /**
     * 添加这个NSocket到缓冲区之后会被调用
     * @param nSocket 添加到管理缓冲区的NSocket
     */
    void afterAdded(NSocket nSocket);

    /**
     * 从管理缓冲区移除此token,此userId的NSocket之前被调用
     * @param token 令牌
     * @param userId 客户端标记
     */
    void beforeRemoved(String token, String userId);

    /**
     * 从管理缓冲区移除此token,此userId的NSocket之后被调用
     * @param token 令牌
     * @param userId 客户端标识
     */
    void afterRemoved(String token, String userId);

    /**
     * 重连之前被调用
     * @param nSocket 新的NSocket
     * @param oldNSocket 旧的NSocket
     */
    void onReconnect(NSocket nSocket, NSocket oldNSocket);
}
