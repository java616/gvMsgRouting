package com.gvsoft.communication.server.serverstatus;

import com.gvsoft.communication.net.NSocket;

/**
 * Created with IntelliJ IDEA.
 * ProjectName:MService
 * Author: zhaoqiubo
 * Date: 15/10/19
 * Time: 下午4:17
 * Desc: NSocket状态监听默认实现类
 */
public class DefaultStatusHandle implements IStatusManageHandle {

    public void beforeAdded(NSocket nSocket) {

    }

    public void afterAdded(NSocket nSocket) {

    }

    public void beforeRemoved(String token,String userId) {

    }

    public void afterRemoved(String token,String userId) {

    }

    public void onReconnect(NSocket nSocket,NSocket oldNSocket){

    }
}
