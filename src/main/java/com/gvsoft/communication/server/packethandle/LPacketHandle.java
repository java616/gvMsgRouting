package com.gvsoft.communication.server.packethandle;

import com.gvsoft.communication.net.model.IPacket;
import com.gvsoft.communication.net.NSocket;
import com.gvsoft.communication.common.packethandle.PacketHandle;
import com.gvsoft.communication.server.socket.NSocketManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created with IntelliJ IDEA.
 * ProjectName:gvMsgRouting
 * Author: zhaoqiubo
 * Date: 15/9/8
 * Time: 下午5:29
 * Desc: 登录报文处理类
 */
public class LPacketHandle extends PacketHandle {
    private final static Logger logger = LogManager.getLogger("server");
    public void handlePacket(IPacket iPacket,NSocket nSocket) {

        nSocket.releaseUserId(iPacket.getPacketBody());
        if(nSocket.getUserId()!=null) {
            NSocketManager.addConn2Cache(nSocket);
            NSocketManager.removeUnregNSocket(nSocket.getToken());
            logger.info("用户：" + nSocket.getUserId() + "已登录，通道为："+nSocket.getChannel()+"UserCount：" + NSocketManager.getTokenMapSize());
        }
        super.handlePacket(iPacket, nSocket);
    }
}
