package com.gvsoft.communication.server.packethandle;

import com.gvsoft.communication.net.model.IPacket;
import com.gvsoft.communication.net.NSocket;
import com.gvsoft.communication.common.packethandle.PacketHandle;

/**
 * Created with IntelliJ IDEA.
 * ProjectName:gvMsgRouting
 * Author: zhaoqiubo
 * Date: 15/9/8
 * Time: 下午5:57
 * Desc: 接受R报文处理类
 */
public class RPacketHandle extends PacketHandle {
    public void handlePacket(IPacket iPacket,NSocket nSocket) {
        //do nothing
    }
}
