package com.gvsoft.communication.server.gather;

import com.gvsoft.communication.console.IControl;
import com.gvsoft.communication.server.Server;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * ProjectName:MService
 * Author: zhaoqiubo
 * Date: 15/11/11
 * Time: 下午3:33
 * Desc: 服务关闭控制类
 */
public class ShutDownController implements IControl {
    public ShutDownController() {
    }

    @Override
    public String execute() {
        try {
            Server.getInstance().close();
            Server.getInstance().exit();
        } catch (IOException e) {
            e.printStackTrace();
            return "关闭服务异常!错误:"+e.getMessage();
        }
        return "服务已经关闭!";
    }

    @Override
    public void close() throws IOException {
        //nothing
    }
}
