package com.gvsoft.communication.server.gather;

import com.gvsoft.communication.console.IControl;
import com.gvsoft.communication.server.net.socket.NSocket;
import com.gvsoft.communication.server.net.socket.NSocketManager;
import com.sun.corba.se.impl.encoding.OSFCodeSetRegistry;

import java.io.IOException;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * ProjectName:MService
 * Author: zhaoqiubo
 * Date: 15/11/12
 * Time: 下午4:09
 * Desc: 连接状态收集器
 */
public class NSocketStatusGather implements IControl {
    @Override
    public String execute() {
        StringBuffer sb = new StringBuffer();
        int connCount = NSocketManager.getConnCache().size();
        sb.append("连接状态收集器打印结果:\r\n").append("gvServer当前连接数量为:<").append(connCount).append(">\r\n");
        for (Map.Entry<String, NSocket> entry : NSocketManager.getConnCache().entrySet()) {
            sb.append("连接token:<"+entry.getKey()+">,连接身份标记:<"+
                    entry.getValue().getUserId()+
                    ">,连接通道信息:<"+entry.getValue().getChannel()+">\r\n");
        }
        return sb.toString();
    }

    @Override
    public void close() throws IOException {

    }
}
