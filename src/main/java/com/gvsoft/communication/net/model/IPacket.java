package com.gvsoft.communication.net.model;

/**
 * Created with IntelliJ IDEA.
 * ProjectName:gvMsgRouting
 * Author: zhaoqiubo
 * Date: 15/8/7
 * Time: 下午1:52
 * Desc: 报文接口
 */
public interface IPacket {
    String getHeader();

    String getRid();

    String getClientToken();

    String getPacketBody();

}
