package com.gvsoft.communication.net.model;

/**
 * Created with IntelliJ IDEA.
 * ProjectName:gvMsgRouting
 * Author: zhaoqiubo
 * Date: 15/8/7
 * Time: 下午1:53
 * Desc: 指令接口
 */
public interface IOrder {

    String generateOrderStr();

    String getHeader();

}
