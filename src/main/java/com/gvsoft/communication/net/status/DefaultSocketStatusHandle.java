package com.gvsoft.communication.net.status;

import com.gvsoft.communication.net.NSocket;

/**
 * Created with IntelliJ IDEA.
 * ProjectName:gvMsgRouting
 * Author: zhaoqiubo
 * Date: 15/10/16
 * Time: 上午10:05
 * Desc: 默认状态监听类
 */
public class DefaultSocketStatusHandle implements ISocketStatusHandle {
    @Override
    public void afterClosed(NSocket nSocket) {

    }

    @Override
    public void afterActived(NSocket nSocket) {

    }

    @Override
    public void afterNew(NSocket nSocket) {

    }

    @Override
    public void afterReconnect(NSocket nSocket) {

    }

    @Override
    public void afterConnected(NSocket nSocket) {

    }
}
