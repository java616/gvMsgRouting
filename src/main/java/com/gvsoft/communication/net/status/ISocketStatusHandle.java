package com.gvsoft.communication.net.status;

import com.gvsoft.communication.net.NSocket;

/**
 * Created with IntelliJ IDEA.
 * ProjectName:gvMsgRouting
 * Author: zhaoqiubo
 * Date: 15/10/16
 * Time: 上午9:33
 * Desc: 状态处理监听接口
 */
public interface ISocketStatusHandle {
    void afterClosed(NSocket nSocket);//连接的初始状态，没有进行物理连接，没有发送身份标记

    void afterActived(NSocket nSocket);//物理连接已经建立，身份标记已经发送，可以进行后续业务操作

    void afterNew(NSocket nSocket);//物理连接已经建立，但是尚未发送身份标记

    void afterReconnect(NSocket nSocket);//物理连接已经关闭，进入重连状态

    void afterConnected(NSocket nSocket);
}
