package com.gvsoft.communication.net.adapter;

import com.gvsoft.communication.net.IoBuffer;
import com.gvsoft.communication.net.model.IOrder;
import com.gvsoft.communication.net.NSocket;
import java.nio.ByteBuffer;

/**
 * Created with IntelliJ IDEA.
 * ProjectName:gvMsgRouting
 * Author: zhaoqiubo
 * Date: 15/9/22
 * Time: 上午11:21
 * Desc: IO适配器接口，可以根据项目情况进行通讯包的解析组合工作，实现此接口
 */
public interface IOAdapter {
    void analyWrite(IOrder iOrder,ByteBuffer byteBuffer);
    void onError(NSocket nSocket,Throwable t);
    String encode(String s);
    String decode(String s);
    Object analyRead(IoBuffer buf);
}
