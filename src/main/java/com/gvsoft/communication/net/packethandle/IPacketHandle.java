package com.gvsoft.communication.net.packethandle;

import com.gvsoft.communication.net.model.IPacket;
import com.gvsoft.communication.net.NSocket;

/**
 * Created with IntelliJ IDEA.
 * ProjectName:gvMsgRouting
 * Author: zhaoqiubo
 * Date: 15/9/8
 * Time: 下午5:27
 * Desc: 报文处理接口
 */
public interface IPacketHandle {
    void handlePacket(IPacket iPacket,NSocket nSocket);
}
