package com.gvsoft.communication.client.packethandle;

import com.gvsoft.communication.client.Client;
import com.gvsoft.communication.net.NSocket;
import com.gvsoft.communication.net.model.IPacket;
import com.gvsoft.communication.common.packethandle.PacketHandle;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created with IntelliJ IDEA.
 * ProjectName:gvMsgRouting
 * Author: zhaoqiubo
 * Date: 15/9/11
 * Time: 上午9:26
 * Desc: 处理来自服务端的R报文
 */
public class RServerPacketHandle extends PacketHandle {
    private final static Logger logger = LogManager.getLogger("client");
    public void handlePacket(IPacket packet, NSocket nSocket) {
        super.handlePacket(packet, nSocket);
        if((!nSocket.isActive()) && packet.getRid().equals(nSocket.getLrid())){

            nSocket.setActive();

        }
        Client client = Client.getClientMap().get(nSocket.getUserId());
        if(client.getKOrderMap().containsKey(packet.getRid())){
            client.clearKeepOrder();
        }
    }
}
