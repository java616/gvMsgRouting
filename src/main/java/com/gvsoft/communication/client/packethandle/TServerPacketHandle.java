package com.gvsoft.communication.client.packethandle;

import com.gvsoft.communication.net.NSocket;
import com.gvsoft.communication.net.model.IPacket;
import com.gvsoft.communication.common.model.order.LoginOrder;
import com.gvsoft.communication.net.packethandle.IPacketHandle;
import com.gvsoft.communication.common.packethandle.PacketHandle;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created with IntelliJ IDEA.
 * ProjectName:gvMsgRouting
 * Author: zhaoqiubo
 * Date: 15/9/10
 * Time: 下午4:05
 * Desc: 处理服务端的T报文,接受服务端分配的token.
 */
public class TServerPacketHandle extends PacketHandle implements IPacketHandle {

    private final static Logger logger = LogManager.getLogger("client");
    public void handlePacket(IPacket packet, NSocket nSocket) {
        super.handlePacket(packet, nSocket);
        nSocket.setToken(packet.getClientToken());
        //构造登录指令,发送给服务端
        LoginOrder loginOrder = new LoginOrder(nSocket.getUserId(), nSocket.getToken());
        nSocket.setLrid(loginOrder.getRid());
        nSocket.write(loginOrder);
    }
}
