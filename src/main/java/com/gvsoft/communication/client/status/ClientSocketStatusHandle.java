package com.gvsoft.communication.client.status;

import com.gvsoft.communication.net.NSocket;
import com.gvsoft.communication.net.status.ISocketStatusHandle;

/**
 * Created with IntelliJ IDEA.
 * ProjectName:gvMsgRouting
 * Author: zhaoqiubo
 * Date: 15/10/16
 * Time: 上午10:05
 * Desc: 默认状态监听类
 */
public class ClientSocketStatusHandle implements ISocketStatusHandle {
    @Override
    public void afterClosed(NSocket nSocket) {
        nSocket.setReconnect();
    }

    @Override
    public void afterActived(NSocket nSocket) {

    }

    @Override
    public void afterNew(NSocket nSocket) {

    }

    @Override
    public void afterReconnect(NSocket nSocket) {

    }

    @Override
    public void afterConnected(NSocket nSocket) {

    }
}
