package com.gvsoft.communication.client.gather;

import com.gvsoft.communication.client.Client;
import com.gvsoft.communication.console.IControl;

import java.io.IOException;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * ProjectName:MService
 * Author: zhaoqiubo
 * Date: 15/11/13
 * Time: 上午10:00
 * Desc: 关闭进程控制器
 */
public class ShutdownController implements IControl {
    @Override
    public String execute() {
       Map<String,Client> clientMap = Client.getClientMap();
        for (Map.Entry<String,Client> entry : clientMap.entrySet()){
            entry.getValue().close();
        }
        System.exit(0);
        return "进程已经停止!";//返回啥也看不到了,进程都停止了.
    }

    @Override
    public void close() throws IOException {

    }
}
