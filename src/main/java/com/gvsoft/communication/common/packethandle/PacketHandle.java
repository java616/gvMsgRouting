package com.gvsoft.communication.common.packethandle;

import com.gvsoft.communication.net.packethandle.IPacketHandle;
import com.gvsoft.communication.net.model.IPacket;
import com.gvsoft.communication.common.model.order.ReplyOrder;
import com.gvsoft.communication.common.model.packet.ReplyPacket;
import com.gvsoft.communication.net.NSocket;

/**
 * Created with IntelliJ IDEA.
 * ProjectName:gvMsgRouting
 * Author: zhaoqiubo
 * Date: 15/9/8
 * Time: 下午5:35
 * Desc: 报文接受处理基类,注释的代码为测试需要代码
 */
public class PacketHandle implements IPacketHandle {

    public void handlePacket(IPacket iPacket, NSocket nSocket) {

        if (!iPacket.getHeader().equals(ReplyPacket.HEADER)) {

            //对客户端的报文做出R相应
            ReplyOrder replyOrder = new ReplyOrder(iPacket.getRid());
            nSocket.write(replyOrder);

        }
    }
}
