package com.gvsoft.communication.common;

import java.util.StringTokenizer;

/**
 * Created with IntelliJ IDEA.
 * ProjectName:MService
 * Author: zhaoqiubo
 * Date: 15/11/20
 * Time: 下午3:38
 * Desc:
 */
public class Tools {
    /**
     * 根据分隔符，将字符串分割为数组
     *
     * @param string       要分割的字符串
     * @param divisionChar 分隔符
     * @return 分割后的字符串数组
     */
    public static String[] str2ArrayByChar(String string, String divisionChar) {
        int i = 0;
        StringTokenizer tokenizer = new StringTokenizer(string, divisionChar);

        String[] str = new String[tokenizer.countTokens()];

        while (tokenizer.hasMoreTokens()) {
            str[i] = tokenizer.nextToken();
            i++;
        }

        return str;
    }
}
