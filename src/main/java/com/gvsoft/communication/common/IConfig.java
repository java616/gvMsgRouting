package com.gvsoft.communication.common;

import com.gvsoft.communication.net.adapter.IOAdapter;
import com.gvsoft.communication.net.status.ISocketStatusHandle;

/**
 * Created with IntelliJ IDEA.
 * ProjectName:MService
 * Author: zhaoqiubo
 * Date: 15/11/20
 * Time: 下午4:22
 * Desc:
 */
public interface IConfig {
    String getProperty(String cfgStr);
    Integer getListenerCount();
    Integer getPort();
    String getIp();
    Integer getReadHandleThreadCount();
    IOAdapter getIoAdapter();
    ISocketStatusHandle getSocketStatusHandle() ;
}
