package com.gvsoft.communication.common.model.packet;

import com.gvsoft.communication.net.model.IPacket;

/**
 * Created with IntelliJ IDEA.
 * ProjectName:gvMsgRouting
 * Author: zhaoqiubo
 * Date: 15/8/13
 * Time: 下午1:22
 * Desc: 客户端登录报文类
 */
public class LoginPacket extends Packet implements IPacket {
    public final static String HEADER = "L";
}
