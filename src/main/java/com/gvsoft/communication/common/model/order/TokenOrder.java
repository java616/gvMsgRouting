package com.gvsoft.communication.common.model.order;

import com.gvsoft.communication.net.model.IOrder;

/**
 * Created with IntelliJ IDEA.
 * ProjectName:gvMsgRouting
 * Author: zhaoqiubo
 * Date: 15/8/6
 * Time: 下午3:09
 * Desc: Token指令类
 */
public class TokenOrder extends Order implements IOrder {

    public static final String HEADER = "T";

    private String token;

    public TokenOrder() {
    }

    public TokenOrder(String token) {
        this.header = HEADER;
        this.token = token;
        this.rid = getNewRid();
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String generateOrderStr() {

            return HEADER + "|" + this.getRid() + "|" + this.getToken();

    }

}
