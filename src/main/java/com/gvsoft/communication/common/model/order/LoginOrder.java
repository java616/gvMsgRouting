package com.gvsoft.communication.common.model.order;

import com.gvsoft.communication.net.model.IOrder;

/**
 * Created with IntelliJ IDEA.
 * ProjectName:gvMsgClient
 * Author: zhaoqiubo
 * Date: 15/8/13
 * Time: 上午11:21
 * Desc: 客户端登录指令类
 */
public class LoginOrder extends Order implements IOrder {
    public final static String HEADER = "L";
    public LoginOrder(String userId, String token){
        this.header = HEADER;
        this.rid = getNewRid();
        this.body = userId;
        this.token = token;
    }
}
