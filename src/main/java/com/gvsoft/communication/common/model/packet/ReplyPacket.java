package com.gvsoft.communication.common.model.packet;

import com.gvsoft.communication.net.model.IPacket;

/**
 * Created with IntelliJ IDEA.
 * ProjectName:gvMsgRouting
 * Author: zhaoqiubo
 * Date: 15/8/7
 * Time: 下午3:44
 * Desc: ack回应包
 */
public class ReplyPacket extends Packet implements IPacket {
    public final static String HEADER="R";

}
