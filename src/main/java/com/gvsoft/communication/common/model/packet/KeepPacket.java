package com.gvsoft.communication.common.model.packet;

import com.gvsoft.communication.net.model.IPacket;

/**
 * Created with IntelliJ IDEA.
 * ProjectName:gvMsgRouting
 * Author: zhaoqiubo
 * Date: 15/8/7
 * Time: 下午3:54
 * Desc: 链路维护包（服务端暂时未使用）
 */
public class KeepPacket extends Packet implements IPacket {
    public final static String HEADER = "K";

}
