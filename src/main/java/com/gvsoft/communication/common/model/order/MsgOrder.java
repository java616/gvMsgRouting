package com.gvsoft.communication.common.model.order;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.gvsoft.communication.net.model.IOrder;

/**
 * Created with IntelliJ IDEA.
 * ProjectName:gvMsgRouting
 * Author: zhaoqiubo
 * Date: 15/8/13
 * Time: 下午4:39
 * Desc: 短消息指令
 */
public class MsgOrder extends Order implements IOrder {

    public final static String HEADER = "M";
    public final static String TYPE_STRING = "S";//文字短消息
    public final static String TYPE_MEDIA = "M";//多媒体短消息

    private String msgBody;

    public MsgOrder(String msgBody) {
        this.header = HEADER;
        this.rid = getNewRid();
        this.msgBody = msgBody;
    }
    public MsgOrder(Object info){
        this(JSON.toJSONString(info, SerializerFeature.WriteMapNullValue));
    }

    public String getMsgBody() {
        return msgBody;
    }

    public String generateOrderStr() {
        StringBuffer sb = new StringBuffer();
        sb.append(HEADER).append("|").append(this.getRid()).append("|").
                append(TYPE_STRING).append("|").append(this.getMsgBody());
        return sb.toString();
    }

}
