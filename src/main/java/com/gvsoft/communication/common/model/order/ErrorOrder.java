package com.gvsoft.communication.common.model.order;

import com.gvsoft.communication.net.model.IOrder;

/**
 * Created with IntelliJ IDEA.
 * ProjectName:gvMsgRouting
 * Author: zhaoqiubo
 * Date: 15/8/7
 * Time: 下午2:36
 * Desc: 错误指令类，有待完善
 */
public class ErrorOrder extends Order implements IOrder {


    private String errCode;
    private String errMsg;
    private String errBody;

    public final static String HEADER = "E";
    public final static String INVAILD_REQ_CODE = "10001";
    public final static String INVAILD_REQ_MSG = "无效请求！";

    public ErrorOrder(String errCode, String errMsg) {
        this.header = HEADER;
        this.errCode = errCode;
        this.errMsg = errMsg;
        this.rid = "0";//没有客户端的rid则默认为0；
        StringBuffer sb = new StringBuffer();
        sb.append("{\"errCode\":").append(errCode)
                .append(",\"errMsg\":").append(errMsg).append("}");
        this.errBody = sb.toString();
    }

    public String getErrBody() {
        return errBody;
    }

    public String generateOrderStr() {

        StringBuffer sb = new StringBuffer();
        sb.append(HEADER).append("|").append(this.getRid())
                .append("|").append(this.getErrBody());
            return sb.toString();

    }
}
