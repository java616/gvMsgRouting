package com.gvsoft.communication.common.model.order;

import org.apache.commons.lang3.StringUtils;

import java.util.UUID;

/**
 * Created with IntelliJ IDEA.
 * ProjectName:gvMsgRouting
 * Author: zhaoqiubo
 * Date: 15/8/7
 * Time: 下午2:01
 * Desc: 指令基类
 */
public abstract class Order {

    //指令头，代表指令的分类
    String header;
    //指令编号
    String rid;

    protected String token;
    protected String body;

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    /**
     * 返回指令序列号，该序列号仅作为响应的对应依据，不唯一，不可作为业务标记。
     *
     * @return
     */
    public static String getNewRid() {
        return UUID.randomUUID().toString().replace("-", "");
    }
    public String generateOrderStr(){

        StringBuffer sb = new StringBuffer();
        sb.append(this.getHeader());

        if(!StringUtils.isBlank(this.getRid())){
            sb.append("|").append(this.getRid());
        }
        if(!StringUtils.isBlank(this.getToken())){
            sb.append("|").append(this.getToken());
        }
        if(!StringUtils.isBlank(this.getBody())){
            sb.append("|").append(this.getBody());
        }

        return sb.toString();

    }
}
