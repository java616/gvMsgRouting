package com.gvsoft.communication.common.model.packet;

import com.gvsoft.communication.net.model.IPacket;

/**
 * Created with IntelliJ IDEA.
 * ProjectName:gvMsgRouting
 * Author: zhaoqiubo
 * Date: 15/8/13
 * Time: 下午3:15
 * Desc: 业务消息包
 */
public class MsgPacket extends Packet implements IPacket {

   public final static String HEADER = "M";

}
