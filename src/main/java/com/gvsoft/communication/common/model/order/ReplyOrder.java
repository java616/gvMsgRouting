package com.gvsoft.communication.common.model.order;

import com.gvsoft.communication.net.model.IOrder;

/**
 * Created with IntelliJ IDEA.
 * ProjectName:gvMsgRouting
 * Author: zhaoqiubo
 * Date: 15/8/7
 * Time: 下午2:27
 * Desc: 响应指令类
 */
public class ReplyOrder extends Order implements IOrder {

    public final static String HEADER = "R";
    public ReplyOrder(){}
    public ReplyOrder(String rid){
        this.header = HEADER;
        this.rid = rid;
    }
    public String generateOrderStr(){
        StringBuffer sb = new StringBuffer();
        sb.append(HEADER).append("|").append(this.getRid());

            return sb.toString();

    }

}
