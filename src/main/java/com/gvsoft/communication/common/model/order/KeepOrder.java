package com.gvsoft.communication.common.model.order;

import com.gvsoft.communication.net.model.IOrder;

/**
 * Created with IntelliJ IDEA.
 * ProjectName:gvMsgClient
 * Author: zhaoqiubo
 * Date: 15/8/7
 * Time: 下午5:05
 * Desc: 客户端维持链路指令类
 */
public class KeepOrder extends Order implements IOrder {
    public static final String HEADER = "K";
    public KeepOrder(String token){
        this.header = HEADER;
        this.token = token;
        this.rid=getNewRid();
    }
}
