package com.gvsoft.communication.console;

import java.io.Closeable;

/**
 * Created with IntelliJ IDEA.
 * ProjectName:MService
 * Author: zhaoqiubo
 * Date: 15/11/11
 * Time: 上午8:56
 * Desc: 控制进程接口,所有的采集器,控制器都需要实现此接口的execute和close方法.
 * 1,execute方法返回的String就是所要打印的系统信息,需要业务端自行进行定义.
 * 2,close方法是考虑在收集系统信息时可能会打开一些需要关闭的内容(比如线程),在close方法中进行释放.
 */
public interface IControl extends Closeable{
    /**
     * execute方法返回的String就是所要打印的系统信息,需要业务端自行进行定义.
     * @return
     */
    public String execute();
}
