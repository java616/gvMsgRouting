package com.gvsoft.communication.console;

import com.gvsoft.communication.console.IControl;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created with IntelliJ IDEA.
 * ProjectName:MService
 * Author: zhaoqiubo
 * Date: 15/11/11
 * Time: 上午9:30
 * Desc: 主控类
 */
public class Console {

    private final static Map<String, IControl> consoleCmdFactory = new ConcurrentHashMap<String, IControl>();

    public static void regCmd(String cmd,IControl cls){
        if (null != cmd && null != cls) {
            consoleCmdFactory.put(cmd, cls);
        }
    }

    public static String execute(String cmd){
        IControl control = consoleCmdFactory.get(cmd);
        if (null == control){
            return "非法命令请求!";
        }else{
            return control.execute();
        }
    }

    public static void close(){
        for(Map.Entry<String,IControl> entry : consoleCmdFactory.entrySet()){
            try {
                entry.getValue().close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }


}
